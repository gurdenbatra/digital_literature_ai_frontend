import { Link } from "react-router-dom";

export default function About() {
	return(
        <div className="dropdown">
            <button className="dropbtn">+</button>
            <div className="dropdown-content">
                <Link to="/">home</Link>	
                <Link to="/about">about</Link>	
                <Link to="/manifesto">manifesto</Link>	
                <Link to="/open-source">open-source</Link>	
                <Link to="/contribute">contribute</Link>	
                <Link to="/credits">credits</Link>	
            </div>
       </div>
	)
};