import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";

import ScrollToTop from "./components/ScrollToTop";

import './App.css'

export default function DigitalLiterature() {
  const labelStyle = {
	color: "#ffffff"
  }
  return (
    <Router>
      <ScrollToTop />
      <Switch>
        <Route path="/">
          <Home/>
        </Route>
      </Switch>
    </Router>
  );
}
