import React, {useEffect, useState} from 'react';

// Package reference: https://github.com/Riley-Brown/react-speech-to-text
import useSpeechToText from 'react-hook-speech-to-text';

const API_KEY = process.env.REACT_APP_GOOGLE_CLOUD_API_KEY;

export default function AnyComponent() {
  
  const [langCode, setLangCode] = useState('en-US');
  const [supportedLang, setSupportedLang] = useState([]);
  const [poem, setPoem] = useState('');
  const [transcript, setTranscript] = useState('');
  const result = '';

  function getLanguages() {
    fetch("https://digital-literature-ai-project.ew.r.appspot.com/get_languages/")
	  .then(response => response.json())
	  .then(data => {
	    const languages = data.data;
            const supportedLanguages = [];

	    for(let i in languages) {
              if(languages[i]["model"] !== "language_not_supported") {
                supportedLanguages.push(languages[i]);
	      }
	    }
	    setSupportedLang(supportedLanguages)
	  })
  }

  useEffect(() => {
    getLanguages();
  },[]);

  function handleSelectChange(event) {
    setLangCode(event.target.value);
  }
 

  // STT package parameters & HTML events	
  const {
    error,
    isRecording,
    results,
    setResults,
    startSpeechToText,
    stopSpeechToText
  } = useSpeechToText({
    continuous: false,
    crossBrowser: true,
    useOnlyGoogleCloud: true,
    googleApiKey: API_KEY,
    useLegacyResults: false,
    googleCloudRecognitionConfig: {
       	languageCode: langCode,
        profanityFilter: false
  }
  });

  useEffect(() => {
    if(results.length > 0) {
      const latestResult = results[results.length - 1]
      setTranscript(latestResult.transcript);
    }
  }, [results]);
  // On STOP button
  /* Stop recording,
   * send the transcript to the API
   * and receive the poem */
   const handleStop = () => { 
    stopSpeechToText();
    console.log(results);
    if(transcript !== ''){
	    var myHeaders = new Headers();
	    myHeaders.append("Content-Type", "application/json");
	    
	    var raw = JSON.stringify({"data":transcript, "user_language":langCode});

	    var requestOptions = {
	      method: 'POST',
	      headers: myHeaders,
	      body: raw,
	      redirect: 'follow'
	    };
	    
	    fetch("https://digital-literature-ai-project.ew.r.appspot.com/make_poem/", requestOptions)
	      .then(response => response.json())
	      .then(result => { 
		setPoem(result);
	      })
	      .catch(error => console.log('error', error));
    } else {
	    console.log('Did not understand speech!');
    }
  };
 
  // On RESET button
  /* clear results & poem */
  const handleReset = () => {
    setResults([]);
    setPoem('');
  }
  
  // just styling
  const labelStyle = {
        color: "#ffffff"
  }
  
  // No support	
  if (error) return <p>Web Speech API is not available in this browser 🤷‍</p>;

  // HTML
  return (
    <div className="main">
	  <label id="langLabel" htmlFor="selectLanguage" style={labelStyle}>Select Language </label>
      <select id="selectLang" name="selectLanguage" onChange={handleSelectChange}>
        {//langs.map(lang => {
      	supportedLang.map(language => {
	  return(
	    <option value={language.code} key={language.code}> {language.language} </option>
	  )
          /*return (
            <option value={lang} key={lang}> {lang} </option>
          )*/
        })}
      </select>
      <button id="start"onClick={startSpeechToText}>Start</button>
      <button id ="stop" onClick={handleStop}>Stop</button>
      <button id="reset" onClick={handleReset}>Reset</button>
      {poem !== '' &&
            <p id="userPoem">{poem.data}</p>
      }
      { transcript !== '' && !isRecording ? <p id="speechUnderstood"></p> : null}
    </div>
  );
}
