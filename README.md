#  {[[(爱 (ài) <3 )\*],[<3 爱 (ài) )\*],[(ài) <3 爱 )\*]]}#

A ML powered system to translates the noise of hatred into a universal “speech-2-<3”.

## Dependencies 
* [React](https://reactjs.org/)
* [p5.js](https://p5js.org/)
* [Tone.js](https://tonejs.github.io)
* [Faust](https://faustdoc.grame.fr/)

## Directory structure 
* /. The root directory holds the main html files and the Faust files
* [scripts/](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/scripts/) Holds the js files
* [css](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/css/) Holds the css style files
* [src/](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/src/) Holds the React files
* [reactjs/dist/](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/reactjs/dist/) Holds the built files of the React App

## HTML/CSS/JS (non-React)
* Clone this repository:
```
git clone https://krrnk@bitbucket.org/gurdenbatra/digital_literature_ai_frontend.git
```
* Change to the repository directory:
```
cd digital_literature_ai_frontend/
```
* Edit the HTML/CSS/JS files
* To test run:
```
python3 -m http.server 8000 --bind 127.0.0.1
```
OR
```
python2 -m SimpleHTTPServer 8000 --bind 127.0.0.1
```
* View the changes in your browser by going to [http://127.0.0.1:8000/](http://127.0.0.1:8000/)
* Commit if happy with the changes

## React
At the moment this project uses react as a simple module for STT. 
The STT process happens in [src/pages/Home.js](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/src/pages/Home.js)

### Editing the React component
* Edit [src/pages/Home.js](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/src/pages/Home.js)
* In [src/index.js](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/src/index.js) change `document.getElementById('stt')` to `document.getElementById('root')`. The function has to look like this:
```
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('stt')
);
```
* Test with `yarn start`.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
* When everything works change [src/index.js](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/src/index.js) back to `document.getElementById('stt')`.
* Build the component files with `yarn build`
* Copy the build files that start with 2.XXXX.chunk.js, main.XXXXXX.chunk.js and runtime-main.XXXXX.chunc.js to [reactjs/dist/](https://bitbucket.org/gurdenbatra/digital_literature_ai_frontend/src/master/reactjs/dist/)
* Edit the index.html file in root folder with the new files that you copied to reactjs/dist/
* Publish

### More about React
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

## Known bugs:
- The current Google Cloud Speech Recognition API is slow. The user needs some visual feedback on speech recognized before pressing stop button.
