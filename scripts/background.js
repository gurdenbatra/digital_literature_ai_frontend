const Y_AXIS = 1;
const X_AXIS = 2;

let navBar;
let logo;

function setGradient(x, y, w, h, c1, c2, axis) {
  
  noFill();
  
  if (axis === Y_AXIS) {
    // Top to bottom gradient
    for (let i = y; i <= y + h; i++) {
      let inter = map(i, y, y + h, 0, 1);
      let c = lerpColor(c1, c2, inter);
      stroke(c);
      line(x, i, x + w, i);
    }
  } else if (axis === X_AXIS) {
    // Left to right gradient
    for (let i = x; i <= x + w; i++) {
      let inter = map(i, x, x + w, 0, 1);
      let c = lerpColor(c1, c2, inter);
      stroke(c);
      line(i, y, i, y + h);
    }
  }
}


/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('#burger')) {
  var myDropdown = document.getElementById("myDropdown");
    if (myDropdown.classList.contains('show')) {
      myDropdown.classList.remove('show');
    }   
  }
}

function scrolling(){
	window.addEventListener('scroll', (event) => {
		resizeCanvas(windowWidth, windowHeight + window.scrollY);
//		skinGradients();
		if(windowWidth >= 768){
        		logo.position(width-300 , height-350);
		} else {
			logo.position(width/2-50, height - 100);
		}			
		
	});


}


function setup(){
	
	canvas = createCanvas(windowWidth, windowHeight);
	canvas.position(0, 0);
	canvas.style('z-index', '-1');

        navBar = new p5.Element(document.getElementById("navbar"));
        logo = new p5.Element(document.getElementById("logo"));
	
	colorArray = [
	    color(254, 235, 231),
	    color(254, 233, 206),
	    color(21, 11, 20),
	    color(25, 11, 0),
	  ];
	
//	skinGradients();
	scrolling();

	navBar.onclick = function(){myFunction();}
}

function draw(){
}


function skinGradients(){
        setGradient(0, 0, width * 0.75, height, colorArray[1], colorArray[2], X_AXIS);
        setGradient(width * 0.75, 0, width * 0.25, height, colorArray[3], colorArray[0], X_AXIS);
}
