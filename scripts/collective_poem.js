function makePoem(requestOptions){
	 fetch("https://digital-literature-ai-project.ew.r.appspot.com/make_poem/", requestOptions)
		.then(response => response.json())
              	.then(data => {
                	console.log('New Poem is: \n' + data.data);
              })
		.catch(error => console.log('error', error));
}

function getPoems(){
	fetch("https://digital-literature-ai-project.ew.r.appspot.com/get_poems/")
	.then(response => response.json())
	.then(data => {
		let poems = data.data.slice(0, -1);
		let poemsContent = [];
		let poemsSplit = [];
		let poemsLines = [];
		let srcContent = document.getElementById("content");

		for(let i=0; i < poems.length; i++){
			poemsContent.push(poems[i].data);
		}
		for(let i=0; i < poemsContent.length; i++){
			poemsSplit.push(poemsContent[i].split('\n'));
		}
		for(let i=0; i < poemsSplit.length; i++){
			for(let j=0; j < poemsSplit[i].length; j++){
				poemsLines.push(poemsSplit[i][j])
			}
		}
		let linesJoined = poemsLines.join("<br>");
		srcContent.innerHTML += linesJoined;
	})
}

function getLastPoem(){
	fetch("https://digital-literature-ai-project.ew.r.appspot.com/get_poems/")
	.then(response => response.json())
	.then(data => {
		let lastPoem = data.data[data.data.length-1].data;
		let lastPoemLang = data.data[data.data.length-1].lang;
		let lastPoemLines = lastPoem.split('\n');
		
		// Write poem on the screen
		for(let i=0; i < lastPoemLines.length; i++){
			document.getElementById("content").innerHTML += ("<br>" + lastPoemLines[i]);
		}
		
		// Request new poem
		let myHeaders = new Headers();
		myHeaders.append("Content-Type", "application/json");
		let raw = JSON.stringify({"data":lastPoemLines[lastPoemLines.length-1], "user_language": lastPoemLang});
		let requestOptions = {
			method: 'POST',
			headers: myHeaders,
			body: raw,
			redirect: 'follow'
		};
		
		makePoem(requestOptions)


	});
}

getPoems();
getLastPoem();
setInterval(getLastPoem, 1400000);
