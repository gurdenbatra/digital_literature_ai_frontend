let userPoem = "igrb e uncon that if I burn this ginglast adjuncted to the song to har again: to ghosts are not here,In this! Hearten: my protection in my body, even just to set on. in feet for the ginstony for time: Old Rays, becracing unsolace for vivid, crying, Narred troll blinkers around. Here Boshokes Councel, The old roses of two because, happy backed and halving, Under the Pose floating pond for all gold"

///////////////// p5.js
let enterBtn;
let enterElement;
let readPoemBtn;
let previousPoemsAmnt = 0;
let poemsAmnt = 0;
let currentPoemsAmnt = 0;
let poemsSinceUsrEntr = 0;
let newPoem = false;
let poemGenerated = false;
let endOfPoem = false;
let mousePrevious = 0;
let mouseCurrent = 0;
let stopWriting = false;
let readingPoem = false;
let writingYou = false;
let speechNotRecognised = false;
let stopTimeOut;
let canvas;
let colorArray;
let colorIndex = 0;
let textColor = '#425F50';
let btnsColor = '#425F50';
let shakeColor = "#fd5e53";
let btnsIconsColor;

let fontRegular, fontItalic, fontBold;

const Y_AXIS = 1;
const X_AXIS = 2;

let navBar;
let logo;
let myFont;

let shakeStr = "s\nh\na\nk\ne\n\nm\ne";
let touchStr = "touch across my surface";
let shaking = false;
let touchFreq = 0;
let touchFreq2 = 0;
let touchAmp = 150;
///////////////// Tone.js 
let hateSamples = [];
let waveSamples = [];
let heartbeatSamples = [];
let demonstrationsSamples = [];
let booingSamples = [];

let sound = false;
let noiseBufLoaded = false;
let booingBufLoaded = false;
let heartBufLoaded = false;
let demonstrationsBufLoaded = false;
let konvolvBufLoaded = false;
let wavesBufLoaded = false;
let lastBufLoaded = false;
let allBufsLoaded = false;
let convolution = false;
let blockchainLoopig = false;
let inmigrationLooping = false;
let blockchainInterval;
let blockchainTimeout;
let inmigrationTimeout;
let inmigrationInterval;
let blockchainWait = 1000;
let majorScale = [130.81, 146.83, 164.81, 174.61, 196.00, 220.00, 246.94, 261.63];
let pentatonicScale = [146.83, 164.81, 196.00, 220, 261.63, 293.66];
let romanianMinorScale = [110, 123.47, 130.81, 155.56, 164.81, 196.00, 220];

let noiseBuffers;
let booingBuffers;
let waveBuffers;
let heartBuffers;
let demonstrationsBuffers;

let noiseEnv;
let heartEnv;
let demonstrationsEnv;
let wavesEnv;
let masterVol;	
let reverb;
let heartVol;
let konvolverVol;
let noiseVol;
let demonstrationsVol;
let wavesVol;	
let noiseGranular; 
let heartPlayer;
let demonstrationsGranular; 
let konvolver;
let wavesPlayer; 

console.log(userPoem);
///////////////// FAUST
if (typeof (WebAssembly) === "undefined") {
	alert("WebAssembly is not supported in this browser, the page will not work !")
    }
var isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
var audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
//let isWebKitAudio;
//let audio_context;
let saw_dsp = null;
let loadFaust = false;

/////////////// REACT
let sttDiv;
let poemElement;
let resetElement;
let startElement;
let stopElement;
let understoodElement;
let languageElement;
let labelElement;
let poemButton;
let resetButton;
let startButton;
let stopButton;
let languageSelect;
let labelp5;

class BufferArray {
	constructor(){
		this.buffers = [];
	}

	newBuffers(path){
		let buffer = new Tone.Buffer(path);
		this.buffers.push(buffer);
		return buffer;
	}

	get allBuffers(){
		return this.buffers;
	} 
}

function preLoadBufs(sampleArray){
	bufArray = new BufferArray();
	for(i=0; i < sampleArray.length; i++){
		bufArray.newBuffers(sampleArray[i]);
	}
	return bufArray
}



function loadAudio(){
	fetch('https://digital-literature-ai-project.ew.r.appspot.com/get_samples/')
		.then(response => response.json())
		.then(data => {
			for(let i in data.data[0]){
				hateSamples.push(data.data[0][i]);
			}

			for(let i in data.data[1]){
				waveSamples.push(data.data[1][i]);
			}

			for(let i in data.data[2]){
				heartbeatSamples.push(data.data[2][i]);
			}

			for(let i in data.data[3]){
				demonstrationsSamples.push(data.data[3][i]);
			}

			for(let i in data.data[4]){
				booingSamples.push(data.data[4][i]);
			}
			
			console.log(hateSamples);
			console.log(booingSamples);
			console.log(waveSamples);
			console.log(demonstrationsSamples);
			console.log(heartbeatSamples);


			noiseBuffers = preLoadBufs(hateSamples);
			booingBuffers = preLoadBufs(booingSamples);
			waveBuffers = preLoadBufs(waveSamples);
			heartBuffers = preLoadBufs(heartbeatSamples);
			demonstrationsBuffers = preLoadBufs(demonstrationsSamples);

			instantiateTone();


			//isWebKitAudio = (typeof (webkitAudioContext) !== "undefined");
			//audio_context = (isWebKitAudio) ? new webkitAudioContext() : new AudioContext();
		})
}

function instantiateTone(){

	noiseEnv = new Tone.AmplitudeEnvelope({
		attack: 0.1,
		decay: 0.2,
		sustain: 1.0,
		release: 5.0
	});

	heartEnv = new Tone.AmplitudeEnvelope({
		attack: 0.1,
		decay: 0.2,
		sustain: 1.0,
		release: 5.0
	});

	demonstrationsEnv = new Tone.AmplitudeEnvelope({
		attack: 0.1,
		decay: 0.2,
		sustain: 1.0,
		release: 5.0
	});

	wavesEnv = new Tone.AmplitudeEnvelope({
		attack: 2,
		decay: 0.5,
		sustain: 1.0,
		release: 10.0
	});


	masterVol = new Tone.Volume(0).toDestination();	
	reverb = new Tone.Reverb().connect(masterVol);
	
	heartVol = new Tone.Volume(0).connect(masterVol);
	konvolverVol = new Tone.Volume(0).connect(masterVol);
	noiseVol = new Tone.Volume(-17).connect(reverb);
	demonstrationsVol = new Tone.Volume(-14).connect(reverb);
	wavesVol = new Tone.Volume(-100).connect(masterVol);	


	noiseGranular = new Tone.GrainPlayer(hateSamples[0], function(){ 
		console.log('noiseBuf loaded!');
		noiseBufLoaded = true;
	}).connect(noiseEnv);

	heartPlayer = new Tone.Player(heartbeatSamples[0], function(){ 
		console.log('heartBuf loaded!');
		heartBufLoaded = true;
	}).connect(heartEnv);

	demonstrationsGranular = new Tone.GrainPlayer(demonstrationsSamples[0], function(){ 
		console.log('demonstrationsBuf loaded!');
		demonstrationsBufLoaded = true;
		lastBufLoaded = true;
	}).connect(demonstrationsEnv);

	let konvolvRandBuf = int(random(0, hateSamples.length)); // int floors the outputthus samples.lenght and not -1
	konvolver = new Tone.Convolver(hateSamples[konvolvRandBuf], function(){
		console.log('konvolvBuf loaded');
		konvolvBufLoaded =  true;
	}).connect(konvolverVol);

	wavesPlayer = new Tone.Player(waveSamples[0], function(){ 
		console.log('wavesBuf loaded!');
		waveBufLoaded = true;
	});
}

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('#burger')) {
  var myDropdown = document.getElementById("myDropdown");
    if (myDropdown.classList.contains('show')) {
      myDropdown.classList.remove('show');
    }   
  }
}

function preload() {
	fontRegular = loadFont('assets/fonts/NotoSans-Regular.ttf');
	fontItalic = loadFont('assets/fonts/NotoSans-Italic.ttf');
	fontBold = loadFont('assets/fonts/NotoSans-Bold.ttf');
}

function setup(){
	
	canvas = createCanvas(windowWidth, windowHeight);
	canvas.position(0, 0);
	canvas.style('z-index', '-1');
	
	enterElement = document.getElementById("enter-btn");
	enterBtn = new p5.Element(document.getElementById("enter-btn"));
	enterBtn.hide();
	enterBtn.position((width/2)-100, height/2);
	
	sttDiv = new p5.Element(document.getElementById("stt")); 
	sttDiv = new p5.Element(document.getElementById("stt")); 
	sttDiv.hide();

	navBar = new p5.Element(document.getElementById("navbar"));
	logo = new p5.Element(document.getElementById("logo"));
	if(windowWidth < 768){
		logo.position(width-100, height-105);
	} else {
		logo.position(width-250, height-200);
	}

	shakeX = random(100, 200);
	shakeY = random(100, 200);

	colorArray = [
	    color(254, 235, 231),
	    color(254, 233, 206),
	    color(21, 11, 20),
	    color(25, 11, 0),
	  ];
	
	intimatePlace();
	
	navBar.onclick = function(){myFunction();}
}

function draw(){
	if(!allBufsLoaded){
		if(lastBufLoaded){
			allBufsLoaded = true;
		}
	}

	poemElement = document.getElementById("userPoem");
	if(poemElement !==  null){
		if(poemGenerated == false){
			enterBtn.hide();
			poemElement.style.display = 'none';
			stopWriting = false;
			userPoem = poemElement.innerText;
			//fetchPoems(userPoem);
			//if(!sound){onOff()};
			poemGenerated = true;
		}

	}

	if(poemGenerated){
		if(frameCount % 10 == 0){
			getPoems();
		}
	}

	if(endOfPoem){
		fetchPoems(userPoem);
	}

	
	languageElement = document.getElementById("selectLang");
	if(languageElement !== null){
		languageSelect = new p5.Element(languageElement);
	}
	
	labelElement = document.getElementById("langLabel");
	if(labelElement !== null){
		labelp5 = new p5.Element(labelElement);
		labelp5.style('font-size', '24px');
		labelp5.style('color', btnsColor);
		labelp5.style('font-type', 'arial');
	}
	
	resetElement = document.getElementById("reset");
	if(resetElement !== null){
		resetButton = new p5.Element(resetElement);
		resetElement.onclick = function(){resetPoem()};
	}

	startElement = document.getElementById("start");
	if(startElement !== null){
		startButton = new p5.Element(startElement);
		startElement.onclick = function(){startRecording()};
	}
	
	stopElement = document.getElementById("stop");
	if(stopElement !== null){
		stopButton = new p5.Element(stopElement);
		stopElement.onclick = function(){stopRecording()};
	}

	understoodElement = document.getElementById("speechUnderstood");
	
	if(understoodElement !== null && !speechNotRecognised){
		if(writingYou){
			drawWritingYou();
			drawTouchMe();
			if(windowWidth <= 768){
				drawShakeMe();
			}
			if(touchFreq2 >= 5){
				touchFreq2 = 0;
				if(poemGenerated){
					fetchPoems(userPoem);
					if(!sound){onOff()};
				}
			}
		}
	} else {
		if(writingYou && !speechNotRecognised){ // on Stop pressed
			didNotUnderstand();
		}
	}

	
}

function windowResized() {
	resizeCanvas(windowWidth, windowHeight);
}

function startPlayer(synth, bool){
	if(synth.state != 'started'){
		if(bool){
			synth.loop = true;
			bool = false;
		}	
		synth.start();
	}
}


function intimatePlace(){
	let textX = 200;
	let textY = 100;
	let safeSize = 64;
	let subSize = 32;
	textIncrement = 50;

	if(windowWidth < 768){
		textX = 10;
		textY = 100;
		safeSize = 42;
		subSize = 21;
	} else if(windowWidth == 768){
		textX = 150;
	}


	clear();
	noStroke();
	fill(textColor);
	textSize(safeSize);
	textStyle(ITALIC);
	textFont('cursive');
	textWrap(WORD);
	text('this is your safe place', textX, textY, windowWidth);
	textSize(subSize);
	textStyle(NORMAL);
	textFont(fontRegular);
	if(windowWidth > 375){
		text('Intimate', textX, textY + 125);
		text('Safe space', textX, textY +175);
		text('(Breath in. Breath out.)', textX, textY + 225);
	} else{
		text('Intimate', textX, textY + 125);
		text('Safe space', textX, textY +150);
		text('(Breath in. Breath out.)', textX, textY + 175);
	}

	enterBtn.show();
	enterElement.onclick = function(){enter()};
}

function enter(){
	let textX = 10;
	let textY = (height/2) - 250;
	let txtSize = 24;
	//let alpha = 0.1;
	let eraser = 0.1;
  enterBtn.hide();
	clear();

	alert("Please use your mobile phone and headphones for a complete experience");
	
	noStroke();
  fill(66, 95, 80, 0)
	textStyle(NORMAL);
	textFont(fontRegular);
	textSize(txtSize);
	textAlign(CENTER, CENTER);
 function fadeInFun (txt, txtY) {
    let alpha = 0.1;
    let fadeIn = setInterval(function () {
      if(alpha >= 1){
        clearInterval(fadeIn);
      } else {
        alpha += alpha * 0.1;
        fill('rgba(66, 95, 80, '+ alpha + ')'); 
        text(txt, textX, textY+txtY, width-10);
      }
    }, 100);
 }
function fadeOutFun () {
    let fadeOut = setInterval(function () {
      if(eraser >= 1){
        clearInterval(fadeOut);
      } else {
        eraser += eraser * 0.1;
        fill('rgba(255, 255, 255, ' + eraser + ')');
        rect(textX, textY-50, width, height);
      }
    }, 100);
}

fadeInFun('1. Think of one word',0);
fadeInFun('from your personal', 35);
fadeInFun('hate-speech experience', 70);
setTimeout(function(){
  fadeInFun('2. Choose your language', 140);
}, 1500);
setTimeout(function(){
  fadeInFun('3. Press Start', 210);
}, 3000);
setTimeout(function(){
  fadeInFun('* Your hate word is not stored anywhere', 280);
}, 4500);
setTimeout(function(){
  sttDiv.show(); 
}, 6000);
setTimeout(fadeOutFun, 9000);

if(windowWidth < 768){
  sttDiv.position((width/2-100), height-275);
} else { 
  sttDiv.position((width/2)-300, height - 150);
}

stopButton.hide();
resetButton.hide();
Tone.start();
Tone.context.resume();



}	

function startRecording(){
let textX = 10;
let textY = (height/2) - 200;
let txtSize = 24;
let textIncrement = 50;

clear();

noStroke();
textStyle(NORMAL);
textFont(fontRegular);
fill(textColor);
textSize(txtSize);
textAlign(CENTER, CENTER);
text('I am listening...', textX, textY, width-10);
  
sttDiv.position((width/2)-100, height - 150);
startButton.hide();
languageSelect.hide();
labelp5.hide();
stopTimeOut = setTimeout(function(){stopButton.show()}, 5500);

if(!allBufsLoaded){
  loadAudio();
}
}

function stopRecording(){
clearTimeout(stopTimeOut);
writingYou = true;
speechNotRecognised = false;

sttDiv.position((width/2)-100, height - 150);
stopButton.hide();
resetButton.show();

startFaustContext();
}

function didNotUnderstand(){
let textX = 10;
let textY = height/2;
let txtSize = 52;
let textIncrement = 50;

clearTimeout(stopTimeOut)
clear();

if(windowWidth < 768){
  txtSize = 24;
} 

noStroke();
fill(textColor);
textAlign(CENTER, CENTER);
textStyle(NORMAL);
textWrap(WORD);
textFont(fontRegular);
textSize(txtSize);
text('Sorry, I did not understand\nwhat you said\n\nPlease, press the reset button\nand speak again', textX, textY, width-10);

speechNotRecognised = true;

}

function drawWritingYou(){
let textX = 10;
let textY = height/2;
let txtSize = 52;
let textIncrement = 50;

clear();

if(windowWidth < 768){
  txtSize = 24;
} 

noStroke();
fill(textColor);
textAlign(CENTER, CENTER);
textStyle(ITALIC);
textFont('cursive');
textSize(txtSize);
text('I am writing you poetry of love\n\nWhile reading the poem\nfeel free to...', textX, textY, width-10);
}

function resetPoem(){
let textX = (width/2);


userPoem = '';
poemGenerated = false;
endOfPoem = false;
stopWriting = true;
readingPoem = false;
speechNotRecognised = false;

clear();
setTimeout(function(){
  clear(); 
  noStroke();
  textStyle(NORMAL);
  textFont(fontRegular);
  fill(textColor);
  textSize(32);
  textAlign(CENTER, CENTER)
  text('Tell me more...', textX, height/2);

  sttDiv.position((width/2)-100, height - 350);
  resetButton.hide();		
  labelp5.show();
  languageSelect.show();
  startButton.show();
}, 100);
}


function shakeMe(str, shake){
fill('#fd5e53')
textSize(42);
textAlign(CENTER, CENTER);
if(shake == true){
  text(str, (width / 2) + randomGaussian(12, 4), (height / 2) + randomGaussian(12, 4));
} else {
  text(str, (width / 2), (height / 2));
}

}

function touchMe(str){
let txtX = width / 2;
let txtY = height / 2;

fill('#fd5e80');
if(windowWidth > 768){
textSize(62);
} else {
  textSize(32);
}
textAlign(CENTER, CENTER);

for(let i in str){
  if(i == 0){
    txtX += (sin(touchFreq) * 50);
    txtY -= (cos(touchFreq) * 50);
    
  }
  if(txtX > 10 && txtX < width-10){
    if(txtY > 0 && txtY < height){
      text(str[i], txtX + (sin(touchFreq * (i * 0.1))* touchAmp), txtY + (cos(touchFreq-(i*0.1)) * (touchAmp + 100)))
    }
  }
}
}

function drawShakeMe(){
if(frameCount % int(random(2, 30)) == 0){
  shakeMe(shakeStr, true); 
} else {
  shakeMe(shakeStr, false)
}
}

function drawTouchMe(){
//clear();
touchMe(touchStr);
touchFreq += 0.006;
  touchFreq = touchFreq % 10;
touchFreq2 += 0.006;
}

function onOff() {
if(!loadFaust){
  startFaust();

  loadFaust = true;

}

if(!sound && lastBufLoaded && loadFaust){
  // Sound on
  allBufsLoaded = true;
  blockchainLoopig = false;

  masterVol.volume.rampTo(0, 1);

  heartbeat();


  if(!blockchainLoopig){
    blockchainTimeout = sleepBlockchain(3000);
    blockchainInterval = loopBlockchain(60000);
  }

  if(!inmigrationLooping){
    inmigrationTimeout = sleepInmigration(100);
    inmigrationInterval = loopInmigration(121000)
  }

  if(saw_dsp !== null){

    saw_dsp.setParamValue("/saw_synth/amp", 2);
  }

  sound = true;
} else{
  //Sound off


  /*noiseGranular.stop();
  heartPlayer.stop();
  wavesPlayer.stop();
  demonstrationsGranular.stop();
  clearTimeout(blockchainTimeout);
  clearInterval(blockchainInterval);
  clearTimeout(inmigrationTimeout);
  clearInterval(inmigrationInterval);
  blockchainLoopig = false;
  inmigrationLooping = false;

  saw_dsp.setParamValue("/saw_synth/amp", 0.0001);

  masterVol.volume.rampTo(-100, 1);
  */	
  sound = false;
}

//	console.log('Sound is:' + sound);

}

//let heartBufCount = 0;
function heartbeat(){
startPlayer(heartPlayer, heartBufLoaded);
heartEnv.triggerAttackRelease(10.0);
heartVol.volume.value = 0;
heartPlayer.chain(heartEnv, heartVol);
}

function demonstrate(){
let randBuf = int(random(0, demonstrationsBuffers.buffers.length)); // int floors the outputthus samples.lenght and not -1
demonstrationsGranular.buffer = demonstrationsBuffers.buffers[randBuf];
startPlayer(demonstrationsGranular, demonstrationsBufLoaded);
demonstrationsEnv.triggerAttackRelease(15.0);
demonstrationsGranular.chain(demonstrationsEnv, demonstrationsVol, masterVol);
konvolverVol.volume.rampTo(-100, 1);
}

function waves(){
let randBuf = int(random(0, waveBuffers.buffers.length)); // int floors the outputthus samples.lenght and not -1
wavesPlayer.stop();
wavesPlayer.buffer = waveBuffers.buffers[randBuf];
startPlayer(wavesPlayer, wavesBufLoaded);
wavesPlayer.loop = false;
wavesEnv.triggerAttackRelease(10.0);
wavesPlayer.chain(wavesEnv, wavesVol);
wavesVol.volume.value = -10;
konvolverVol.volume.rampTo(-100, 2);
console.log("Waves ON");
}

function triggerNoise(grainSize, overlap, bufArray){
let sustain = 0.5;
if(bufArray == noiseBuffers){
  sustain = random(0.001, 0.1);
} else if (bufArray == booingBuffers){
  sustain = random(2.0, 4.0);
}
let randBuf = int(random(0, bufArray.buffers.length)); // int floors the outputthus samples.lenght and not -1
noiseGranular.stop();
noiseGranular.buffer = bufArray.buffers[randBuf];
startPlayer(noiseGranular, noiseBufLoaded)
noiseEnv.triggerAttackRelease(sustain);
noiseGranular.granSize = grainSize;
noiseGranular.overlap = overlap;

noiseGranular.chain(noiseEnv, noiseVol, reverb, konvolver);
convolution = true;
}

function mouseMoved(){
mouseCurrent += 1;
let mouseDiff = mouseCurrent - mousePrevious;
mousePrevious = mouseCurrent;

if(mouseDiff > 0){
  if(sound && lastBufLoaded){
    if(heartEnv.value == 0){
      heartbeat();
    }
  }
} 
}

function deviceShaken(){
if(sound && lastBufLoaded){
  waves();
}
}


//////////////// FAUST
function startFaust()
{
// Create the Faust generated node
var pluginURL = ".";
var plugin = new Faustsaw_synth(audio_context, pluginURL);
plugin.load().then(node => {
		saw_dsp = node;
		console.log(saw_dsp.getJSON());
		// Print path to be used with 'setParamValue'
		console.log(saw_dsp.getParams());
		// Connect it to output as a regular WebAudio node
		saw_dsp.connect(audio_context.destination);
	}); 
}

// Load faust synth
function startFaustContext(){

	// To activate audio on iOS
	window.addEventListener('touchstart', function() {
				if (audio_context.state !== "suspended") return;
				// create empty buffer
				var buffer = audio_context.createBuffer(1, 1, 22050);
				var source = audio_context.createBufferSource();
				source.buffer = buffer;

				// connect to output (your speakers)
				source.connect(audio_context.destination);

				// play the file
				source.start();

				audio_context.resume().then(() => console.log("Audio resumed"));
				}, false);

	// On desktop
	window.addEventListener("mousedown", () => {
	    if (audio_context.state !== "suspended") return;
	    audio_context.resume().then(() => console.log("Audio resumed"))
	});

	if (audio_context.state !== "suspended") return;
	audio_context.resume().then(() => console.log("Audio resumed"))
}


////////////////// BLOCKCHAIN
function delay(ms){
	return new Promise(resolve => setTimeout(resolve, ms));
}

function findMax(array){
	/* According to https://stackoverflow.com/a/30334278 
     	it provides the best performance */
	let max = array[0];
	for(let i=0; i<array.length; i++){
       		if(array[i] > max){	
  	     		max = array[i]
       		}
    	}
	return max;
}


function findMin(array){
	/* According to https://stackoverflow.com/a/30334278 
     	it provides the best performance */
	let min = array[0];
	for (let i=0; i<array.length; i++){
       		if(array[i] < min){	
  	     		min = array[i]
       		}
    	}
	return min; 
}

function closeTo(value, min, avg, max){
	if(value >= min && value < avg){
		let minDiff = value - min;
		let avgDiff = avg - value;
		if(minDiff < avgDiff){
			return 0;
		} else {

			return 1;
		}
	} else if(value >= avg && value <= max){
		let avgDiff = value - avg;
		let maxDiff = max - value;
		if(avgDiff < maxDiff){
			return 1;
		} else {
			return 2;
		}
	}
}

// Add addresses param once server response is done
function trackAddresses(){
//	fetch('https://blockchain.info/multiaddr?active=' + addresses)
	fetch("https://blockchain.info/multiaddr?active=1JTeYcsx37XTq5NRgjepAHDqaLHTZUL88a|bc1q6jw4a4xnwhrprtqjyp4hw4c24esftf2apd8a25|1DQHohS9xUDoPt2WnABRhCCAovjVHRQv4d|1Fd8RuZqJNG4v56rPD1v6rgYptwnHeJRWs|1Ej6qC5zuNAz4xhBjowxDWD9XRjzSpadVm|1ChE5DZVVZJpv8mnJ3fRrtSDrTikBh7uFL|18Kgfgpd1PqSiyaxdWzvz3jLaBPYeKDSyL|1PYcVEnJPG42XYEhhMiwa4KLPkoWYjVmtG")
		.then(response => response.json())
		.then(data => {
			let txsSizes = [];
			let txsBalances = [];
			for(let i in data.txs){
				txsSizes.push(data.txs[i].size);
				txsBalances.push(data.txs[i].balance);
			}
			let txsMaxSize = findMax(txsSizes);
			let txsMinSize = findMin(txsSizes);
			let txsMaxBalance = findMax(txsBalances);
			let txsMinBalance = findMin(txsBalances);
			let sumSize = txsSizes.reduce(function(a, b){
				return a+b;
			})
			let avgSize = sumSize / txsSizes.length;
			console.log('Min txs size is' + txsMinSize);
			console.log('Max txs size is' + txsMaxSize);
			console.log('Blockchain txs loop');
			
			(async function(){
				let previousWeight = 0;
				let counter = 0;
				for(let i in data.txs){
					counter += 1;
					//console.log(data.txs[i]);
					convolution = false;
					blockchainWait = random(2000.0, 5000.0)
					if(!convolution && wavesEnv.value == 0){
						let txsGrain = data.txs[i].balance / 100000000; // amount in btc
						let txsOverlap = map(txsSizes[i], txsMinSize, avgSize, 0.01, 2.0, true);
						let txsLFOFreq = map(data.txs[i].balance, txsMinBalance, txsMaxBalance, 0.1, 2.3333);
						let txsDetune = 146.83 + (data.txs[i].result / 10000000); // ask Liisi if -1 zero OR -2 zeros
						if(data.txs[i].size < avgSize){
							triggerNoise(txsGrain, txsOverlap, noiseBuffers);
						} else {

							triggerNoise(txsGrain, txsOverlap, booingBuffers);
						}
						saw_dsp.setParamValue("/saw_synth/lfo_freq", txsLFOFreq);
						if(demonstrationsEnv.value > 0){
							demonstrationsGranular.grainSize = txsGrain;
							demonstrationsGranular.overlap = txsOverlap;
							reverb.decay = 0.8;
							konvolverVol.volume.rampTo(-100, 1);
						} else {
							konvolverVol.volume.value = map(data.txs[i].balance, txsMinBalance, txsMaxBalance, -18, -6);
							reverb.decay = map(txsSizes[i], txsMinSize, avgSize, 0.1, 0.8, true);
						}
					} else {
						if(noiseEnv.value == 0){
							noiseGranular.disconnect();
						}
						convolution = false;
					}

					blockchainLoopig = true;
					await delay(428.571428578);
				}

			})();

		})
		.catch(err => console.error(err));
}

function randWalk(min, max, step, start){
	let randVal = random();
	let walk = start;

	if(walk < min){
		return min;
	} else{

		if(randVal < 0.5){
			if(walk >= min){
				walk -= step
			}
		} else {
			if(walk <= max){
				walk += step
			}
		}

		console.log('walk is' + walk);
		return randWalk(min, max, step, walk);

		//return walk;

	}

}

function walkScale(scale){
	let step = int(random(1, 3));
	let note = scale[randWalk(0, scale.length-1, step, 0)];
	console.log('note is: ' + note);
	return note;
}

function scaleWalk(randNum, walkStep, walker, scaleArray){
	if(walker >= 0){
		walker += (walkStep * randNum);
		walker = walker % scaleArray.length;
	}  
	
	if(walker >= 0){
		return walker
	} else {
		return 0;
	}
}

function inmigrationScales(){
	let walk = 0;
	fetch('assets/total_inmigration_to_finland.json')
		.then(response => response.json())
		.then(jsondata => {
			let inmigrationValues = [];
			for(let i in jsondata.data){
				inmigrationValues.push(float(jsondata.data[i].values[0]));
			}
			let inmigrationMax = findMax(inmigrationValues);
			let inmigrationMin = findMin(inmigrationValues);
			let inmigrationSum = inmigrationValues.reduce(function(a, b){
				return a + b;
			}, 0)
			let inmigrationAvg = inmigrationSum / inmigrationValues.length;
			console.log('inmigration loops');
			(async function(){
				for(let i in inmigrationValues){
					let scaleClass = closeTo(inmigrationValues[i], inmigrationMin, inmigrationAvg, inmigrationMax)
					let coin = random([-1, 1]);
					let step = int(random(2)) + 1;
					//console.log('scale class is: '+ scaleClass);
					switch(scaleClass){
						case 0:
							walk = scaleWalk(coin, step, walk, majorScale);
							let note0 = majorScale[walk];
							saw_dsp.setParamValue("/saw_synth/fund_freq", note0);
							break;
						case 1:
							walk = scaleWalk(coin, step, walk, romanianMinorScale);
							let note1 = romanianMinorScale[walk];
							saw_dsp.setParamValue("/saw_synth/fund_freq", note1);
							break;
						case 2:
							walk = scaleWalk(coin, step, walk, pentatonicScale);
							let note2 = pentatonicScale[walk];
							saw_dsp.setParamValue("/saw_synth/fund_freq", note2);
							break;
					}

					inmigrationLooping = true;
					await delay(4000);
				}
			}());
		});
}

function inmigrationEurope(){
	fetch('https://ec.europa.eu/eurostat/wdds/rest/data/v2.1/json/en/tps00176?precision=1')
		.then(response => response.json())
		.then(data => {
			let years = data.dimension.time.category.index;
			let countries = data.dimension.geo.category.index;
			let inmigration = data.value;
			let countriesCounter = 0;
			let yearsCounter = 0;
			let countriesInmigration = [];
			
			for (let i in inmigration) {
				
				if(countriesCounter < Object.keys(countries).length){
					countriesInmigration.push(inmigration[i]);
					console.log("//////\nCountries counter: " + countriesCounter);
					if(countriesCounter == 0){

						countriesInmigration = [];

						yearsCounter += 1;
						console.log("******\nYears counter: " + yearsCounter);
						//yearsCounter = yearsCounter % Object.keys(years).length;
					}

				}	

				countriesCounter += 1;
				countriesCounter = countriesCounter % Object.keys(countries).length;
				//let inmigrationSum = countriesInmigration.reduce(function(a, b){
				//	return a + b;
				//}, 0);
				//let inmigrationAvg = inmigrationSum / countries.lenght;
				//console.log("Avg inmigration per year is: " + inmigrationAvg);
			}
		});
}


function sleepBlockchain(ms){
	// Add addresses param once server response is done
	return setTimeout(function(){ trackAddresses()  }, ms);
}
function loopBlockchain(ms){
	// Add addresses param once server response is done
	return setInterval(function(){ trackAddresses()  }, ms);
}

function sleepInmigration(ms){
	return setTimeout(function(){ inmigrationScales()  }, ms);
}
function loopInmigration(ms){
	return setInterval(function(){ inmigrationScales()  }, ms);
}

/////////////////////// POEMS
function fetchPoems(genPoem){
	writingYou = false;
	endOfPoem = false;
	readingPoem = true;	
	sttDiv.position((width/2)-100, height - 150);
	clear();
	let poem = genPoem;
	let textX = 50;
	let textY = height * 0.1;
	let xCumulative = 30;
	let yCumulative = 60;
	let lineBreak = 70;
	let poemSize = 22;
	if(windowWidth < 768){
		textX = 10;
		poemSize = 18;
		xCumulative = 15;
		yCumulative = 30;
		lineBreak = 40;
	}
	console.log(poem);
	(async function(){
		for(let i in poem){
			if(textX < width-80){
				textX += xCumulative;

				if(poem[i] == "\n"){
					if(windowWidth < 768){
						textX = 10
					} else {
						textX = 50;
					}
					textY += lineBreak;
				}
			} else {
				if(windowWidth < 768){
					textX = 10
				} else {
					textX = 50;
				}
				textY += yCumulative; 
			}
			noStroke();
			textStyle(NORMAL);
			textFont(fontRegular);
			fill(textColor);
			textSize(poemSize);
			if(wavesEnv.value > 0){
				translate(int(width * random(0.01, 0.75)), int(height * random(0.01, 0.75)));
				shearX(PI * random(-0.25, 0.5));
				shearY(PI * random(-0.25, 0.5));
				if(poem[i] != "\n"){
					text(poem[i], textX, textY);
				}
			}
			if(wavesEnv.value == 0){
				if(poem[i] != "\n"){
					text(poem[i], textX, textY);
				}
			}
			if(i == poem.length-1){
				console.log('END OF POEM');
				endOfPoem = true;
				readingPoem = false;
			}

			if(stopWriting == true){
				break;
			}
			await delay(428.571428578 * 0.25)
		}
	}());
}

/////////////////////// GET POEMS 

function getPoems(){
	fetch('https://digital-literature-ai-project.ew.r.appspot.com/get_poems/')
		.then(response => response.json())
		.then(data => {
			poemsAmnt = data.data.length;

			previousPoemsAmnt = currentPoemsAmnt;
			currentPoemsAmnt = poemsAmnt;
			if(currentPoemsAmnt > previousPoemsAmnt){
				if(!newPoem){
					console.log('New poem! Current number is:' + poemsAmnt);
					if(sound && lastBufLoaded){
						demonstrate();
						poemsSinceUsrEntr += 1;
						drawAmntPoemsSinceEntr();
					}
					newPoem = true;
				}
			} else {
				if(newPoem){
					console.log('Same poems amount: ' + poemsAmnt)
					newPoem = false;
				}
			}
		});
	
}

function drawAmntPoemsSinceEntr(){
	noStroke();
	textStyle(NORMAL);
	textFont(fontRegular);
	fill(textColor);
	textSize(32);
	text(poemsSinceUsrEntr, width-150, 70);
}
