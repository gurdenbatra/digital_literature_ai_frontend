import("stdfaust.lib");
import("filters.lib");
import("reverbs.lib");

n = 5;
lfo_freq = hslider("lfo_freq", 0.1, 0.01, 10.0, 0.01) : si.smoo;
lfo = par(i, n, _*(no.lfnoise0( lfo_freq / (i + 1) )));

osc = os.sawtooth;
fund_freq = hslider("fund_freq", 146.83, 110.0, 440.0, 0.1) : si.smoo;
osc_bank = osc(fund_freq), par(i, n-1, osc(fund_freq * (((i + 1) * (5/4)))));
partials_amp = par(i, n, _*(1 / (i + 1))) : par(i, n, _/(sum(i, n, (i + 1)) ));

filters = par(i, n, fi.resonlp(500 * (i +1) : *((os.oscsin( (lfo_freq * 10) / (i + 1)) + 1) *0.5) + 0.01, ((no.lfnoise(i+1) + 1) * 0.5) + 0.01, 1));
lpf = fi.highpass(1, 90);

rev = re.jpverb(15, 0, 5, lfo_freq  * 0.7, 0.01, lfo_freq, 0.1,1,1, 10000, 90);

amp = hslider("amp", 1, 0.00001, 8.00, 0.01) : si.smoo;

process =  osc_bank : partials_amp : lfo : filters :> _ : lpf * amp <: _,_ : rev;
// process = 0;

